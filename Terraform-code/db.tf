module "db" {
  source  = "terraform-aws-modules/rds/aws"
 

  identifier = "mysql-db"

  engine            = "mysql"
  engine_version    = "8.0.35"
  instance_class    = "db.t3.micro"
  allocated_storage = 5

  db_name     = "wine"
  username = "root"
  port = 3306
  
  
  
  
  vpc_security_group_ids = [module.db_server_sg.security_group_id]

  tags = {
    Owner       = "user"
    Environment = "devvv"
  }
  
  multi_az               = false
  create_db_subnet_group = true
  subnet_ids           = [module.vpc.public_subnets[0], module.vpc.public_subnets[1]]
  family               = "mysql8.0"
  major_engine_version = "8.0"

  skip_final_snapshot = true
  deletion_protection = false
  publicly_accessible = true

  parameters = [
    {
      name  = "character_set_client"
      value = "utf8"
    },
    {
      name  = "character_set_server"
      value = "utf8mb4"
    }
  ]
 
}


