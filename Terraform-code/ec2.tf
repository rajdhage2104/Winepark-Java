module "ec2_instance" {
  source  = "terraform-aws-modules/ec2-instance/aws"

  name = "Jenkins-instance"
  ami                    = var.ami
  instance_type          = var.instance_type
  key_name               = var.key_name
  vpc_security_group_ids = [module.vote_service_sg.security_group_id]

  monitoring             = true
  subnet_id              = module.vpc.public_subnets[0]

  tags = {
    Terraform   = "true"
    Environment = "dev"

  
  }
}

module "ec2_instance_2" {
  source  = "terraform-aws-modules/ec2-instance/aws"

  name = "Sonarqube-instance"
  ami                    = var.ami
  instance_type          = var.instance_type
  key_name               = var.key_name
  vpc_security_group_ids = [module.web_server_sg.security_group_id]

  monitoring             = true
  subnet_id              = module.vpc.public_subnets[1]

  tags = {
    Terraform   = "true"
    Environment = "devv"
  }
}

