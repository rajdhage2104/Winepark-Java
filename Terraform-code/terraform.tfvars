region                          = "us-east-1"
name                            = "my-vpc" 
cluster_name                    = "my-cluster"                        
cidr                            = "10.0.0.0/16"                                       
azs                             = ["us-east-1a", "us-east-1b"]          
private_subnets                 = ["10.0.1.0/24", "10.0.2.0/24"]
public_subnets                  = ["10.0.3.0/24", "10.0.4.0/24"]
ami                             = "ami-080e1f13689e07408"
instance_type                   = "t2.medium"
# instance_types                  = ["t2.medium"]
vpc_public_inbound_acl_rules    = [{ rule_number = 100, rule_action = "allow", from_port = 80, to_port = 80, protocol = "tcp", cidr_block = "0.0.0.0/0" }]
vpc_public_outbound_acl_rules   = [{ rule_number = 100, rule_action = "allow", from_port = 80, to_port = 80, protocol = "tcp", cidr_block = "0.0.0.0/0" }]
vpc_private_inbound_acl_rules   = [{ rule_number = 100, rule_action = "allow", from_port = 80, to_port = 80, protocol = "tcp", cidr_block = "0.0.0.0/0" }]
vpc_private_outbound_acl_rules  = [{ rule_number = 100, rule_action = "allow", from_port = 80, to_port = 80, protocol = "tcp", cidr_block = "0.0.0.0/0" }]
vote_service_ingress_with_cidr_blocks = [
    {
      from_port   = 80
      to_port     = 80
      protocol    = "tcp"
      description = "User-service ports"
      cidr_blocks = "0.0.0.0/0"
    },

    {
      from_port   = 8080
      to_port     = 8080
      protocol    = "tcp"
      description = "Jenkins port"
      cidr_blocks = "0.0.0.0/0"
    },

    
    {
      rule        = "postgresql-tcp"
      cidr_blocks = "0.0.0.0/0"
    },
  ]

  web_server_ingress_with_cidr_blocks = [
    {
      from_port   = 80
      to_port     = 80
      protocol    = "tcp"
      description = "User-service ports"
      cidr_blocks = "0.0.0.0/0"
    },

    {
      from_port   = 9000
      to_port     = 9000
      protocol    = "tcp"
      description = "Sonarqube port"
      cidr_blocks = "0.0.0.0/0"
    },

    
    {
      rule        = "postgresql-tcp"
      cidr_blocks = "0.0.0.0/0"
    },
  ]

  
  

  db_server_ingress_with_cidr_blocks = [
    {
      from_port   = 80
      to_port     = 80
      protocol    = "tcp"
      description = "User-service ports"
      cidr_blocks = "0.0.0.0/0"
    },

    {
      from_port   = 3306
      to_port     = 3306
      protocol    = "tcp"
      description = "mysql port"
      cidr_blocks = "0.0.0.0/0"
    },

    
    
  ]
 
  