variable "region" {
  description = "The region name"
  type        = string
}

variable "name" {
  description = "Name of VPC"
  type        = string
}

variable "cluster_name"{
  description = "Name of cluster"
  type = string
}

variable "cidr" {
  description = "CIDR block for VPC"
  type        = string
  }

variable "azs" {
  description = "Availability zones for VPC"
  type        = list(string)
}

variable "private_subnets" {
  description = "A list of private subnets inside the VPC"
  type        = list(string)

}
variable "public_subnets" {
  description = "A list of public subnets inside the VPC"
  type        = list(string)

}

variable "tags" {
  description = "Tags to apply to resources created by VPC module"
  type        = map(string)
  default = {
    Terraform   = "true"
    Environment = "dev"
  }
}

variable "ami" {
  description = "type of instance"
  type = string
}


variable "instance_type" {
  description = "type of instance"
  type = string
}

# variable "instance_types" {
#   description = "List of EC2 instance types for the node group"
#   type        = list(map(string))
# }


variable "key_name" {
  description = "key-name"
  type = string
  default = "sunil"
}

variable "monitoring" {
  description = "monitoring"
  type = string
  default = "true"
}

variable "vpc_public_inbound_acl_rules" {
  description = "Public subnets inbound network ACLs"
  type        = list(map(string))

}
variable "vpc_public_outbound_acl_rules" {
  description = "Public subnets outbond network ACLs"
  type        = list(map(string))

}
variable "vpc_private_inbound_acl_rules" {
  description = "Private subnets inbound network ACLs"
  type        = list(map(string))

}
variable "vpc_private_outbound_acl_rules" {
  description = "Private subnets outbound network ACLs"
  type        = list(map(string))

}

variable "map_public_ip_on_launch" {

  type    = bool
  default = true
}

variable "vote_service_ingress_with_cidr_blocks" {
  description = "Allowed rules"
  type = list(map(string))
}

variable "web_server_ingress_with_cidr_blocks" {
  description = "Allowed rules"
  type = list(map(string))
}




variable "db_server_ingress_with_cidr_blocks" {
  description = "Allowed rules"
  type = list(map(string))
}

