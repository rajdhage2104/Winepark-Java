module "vpc" {
  source = "terraform-aws-modules/vpc/aws"

  name = var.name
  cidr = var.cidr

  azs                      = var.azs
  private_subnets          = var.private_subnets
  public_subnets           = var.public_subnets
  map_public_ip_on_launch  = var.map_public_ip_on_launch
 

  tags = var.tags
}

module "vote_service_sg" {
  source = "terraform-aws-modules/security-group/aws"

  name        = "user-service"
  description = "Security group for user-service with custom ports open within VPC, and PostgreSQL publicly open"
  vpc_id      = module.vpc.vpc_id
  

  # Allow all inbound traffic
  ingress_cidr_blocks      = ["0.0.0.0/0"]
  ingress_rules            = ["all-all"]  # Allow all protocols and ports
  
  ingress_with_cidr_blocks = var.vote_service_ingress_with_cidr_blocks
  egress_cidr_blocks = ["0.0.0.0/0"]
}

module "web_server_sg" {
  source = "terraform-aws-modules/security-group/aws"

  name        = "web-service"
  description = "Security group for user-service with custom ports open within VPC, and PostgreSQL publicly open"
  vpc_id      = module.vpc.vpc_id
  

  # Allow all inbound traffic
  ingress_cidr_blocks      = ["0.0.0.0/0"]
  ingress_rules            = ["all-all"]  # Allow all protocols and ports
  
  ingress_with_cidr_blocks = var.web_server_ingress_with_cidr_blocks
  egress_cidr_blocks = ["0.0.0.0/0"]
}

module "db_server_sg" {
  source = "terraform-aws-modules/security-group/aws"

  name        = "db-service"
  description = "Security group for user-service with custom ports open within VPC, and PostgreSQL publicly open"
  vpc_id      = module.vpc.vpc_id
  

 # Allow all inbound traffic
  ingress_cidr_blocks      = ["0.0.0.0/0"]
  ingress_rules            = ["all-all"]  # Allow all protocols and ports
  
  
  ingress_with_cidr_blocks = var.db_server_ingress_with_cidr_blocks
  egress_cidr_blocks = ["0.0.0.0/0"]
}